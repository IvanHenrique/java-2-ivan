package com.bhlangonijr.service;

import com.bhlangonijr.domain.Message;
import com.bhlangonijr.domain.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Null;
import java.util.ArrayList;
import java.util.List;

@Service
public class MessageService {
    private static final Logger log = LoggerFactory.getLogger(MessageService.class);

    /**
     * Validate and store this message into the database
     *
     * @param message
     * @return
     * @throws Exception
     */
    public Response send(Message message) {
        Response response = new Response();

        if(message.getFrom().equalsIgnoreCase("me@earth")){
            response.setSuccess(true);
        } else {
            response.setSuccess(false);
        }

        // ..... validate message and store

        if (response.getSuccess()) {
            // Teste 1
            response.setText("hello there - has been processed");
        } else {
            // Teste 3
            response.setText("Message cannot be sourced by martians!");
        }

        return response;
    }

    public List<Message> getAllMessages() {

        Message mensagem = new Message();
        mensagem.setId("123");
        mensagem.setFrom("me@earth");
        mensagem.setTo("you@earth");
        mensagem.setText("hello there");

        List<Message> listaMessage = new ArrayList<>();
        listaMessage.add(mensagem);

        return listaMessage;
    }

    public Message getById(String id) {

        Message msg = new Message();

        for (Message m : getAllMessages()){
            if(m.getId().equalsIgnoreCase(id)){
                msg.setId(m.getId());
                msg.setFrom(m.getFrom());
                msg.setTo(m.getTo());
                msg.setText(m.getText());
            }
        }

        return msg;
    }


}
